package com.gfb.log_surfer.filter;

import org.junit.Test;

import java.util.ArrayList;
import java.util.List;

import static org.junit.Assert.assertEquals;

public class FilesScannerTest {
    @Test
    public void testScanWithText() throws Exception {
        final FilterConfiguration configuration = new FilterConfiguration();
        configuration.setQuery("HELLO");
        configuration.setRegex(false);
        FilesScanner target = new FilesScanner(configuration);

        List<MatchPoint> expected = new ArrayList<>();
        expected.add(new MatchPoint(13, 6, 5));
        expected.add(new MatchPoint(13, 11, 5));
        expected.add(new MatchPoint(13, 26, 5));

        List<MatchPoint> actual = target.scanWithText(13, "hello HELLOHELLO wildfowl HELLO");

        assertEquals(expected, actual);
    }

    @Test
    public void testScanWithRegex() throws Exception {
        final FilterConfiguration configuration = new FilterConfiguration();
        configuration.setQuery("[\\[]{1}[\\w]+[\\]]{1}");
        configuration.setRegex(true);
        FilesScanner target = new FilesScanner(configuration);

        List<MatchPoint> expected = new ArrayList<>();
        expected.add(new MatchPoint(13, 6, 6));
        expected.add(new MatchPoint(13, 18, 7));
        expected.add(new MatchPoint(13, 31, 10));
        expected.add(new MatchPoint(13, 49, 7));

        List<MatchPoint> actual = target.scanWithRegex(
                13,
                "hello [INFO] HELLO[DEBUG]HELLO [CRITICAL]wildfowl[ERROR] HELLO"
        );

        assertEquals(expected, actual);
    }
}