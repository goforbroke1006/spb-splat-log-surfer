package com.gfb.log_surfer.view;

import com.gfb.log_surfer.Application;
import com.gfb.log_surfer.filter.FilterConfiguration;
import org.apache.commons.lang.StringUtils;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class FilterSettingsPanel extends JDialog {
    private Application rootFrame;

    private JFileChooser targetDirectoryChooser = new JFileChooser();
    private JTextField queryTextField = new JTextField();
    private JCheckBox regexCheckBox = new JCheckBox("Is regex?");
    private JTextField extensionTextField = new JTextField();

    public FilterSettingsPanel(final Application mainPanel, FilterConfiguration configuration) {
        super(mainPanel);
        this.rootFrame = mainPanel;

        setTitle("Setup search params");
        setSize(new Dimension(320, 320));
        setResizable(false);

        Container container = getContentPane();
        container.setLayout(null);

        JButton openDirectoryDialogBtn = new JButton("Select files directory");
        openDirectoryDialogBtn.setLocation(10, 10);
        openDirectoryDialogBtn.setSize(300, 20);
        openDirectoryDialogBtn.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent actionEvent) {
                targetDirectoryChooser.setFileSelectionMode(JFileChooser.DIRECTORIES_ONLY);
                targetDirectoryChooser.setAcceptAllFileFilterUsed(false);
                targetDirectoryChooser.showOpenDialog(mainPanel);
            }
        });
        targetDirectoryChooser.setSelectedFile(configuration.getTargetDirectory());
        container.add(openDirectoryDialogBtn);

        queryTextField.setText(configuration.getQuery());
        queryTextField.setLocation(10, 40);
        queryTextField.setSize(300, 20);
        container.add(queryTextField);

        regexCheckBox.setSelected(configuration.isRegex());
        regexCheckBox.setLocation(10, 70);
        regexCheckBox.setSize(300, 20);
        container.add(regexCheckBox);

        extensionTextField.setText(StringUtils.join(configuration.getExtensions(), " "));
        extensionTextField.setLocation(10, 100);
        extensionTextField.setSize(300, 20);
        container.add(extensionTextField);


        final FilterSettingsPanel settingsPanel = this;

        JButton closeOkButton = new JButton("Ok");
        closeOkButton.setLocation(220, 250);
        closeOkButton.setSize(90, 20);
        closeOkButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                final FilterConfiguration configuration = new FilterConfiguration();
                configuration
                        .setTargetDirectory(targetDirectoryChooser.getSelectedFile())
                        .setQuery(queryTextField.getText())
                        .setRegex(regexCheckBox.isSelected())
                        .setExtensions(extensionTextField.getText().split("\\W"))
                ;
                rootFrame.setConfiguration(configuration);
                rootFrame.updateFilesTree();
                settingsPanel.dispose();
            }
        });
        container.add(closeOkButton);

        JButton closeCancelButton = new JButton("Cancel");
        closeCancelButton.setLocation(120, 250);
        closeCancelButton.setSize(90, 20);
        closeCancelButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                settingsPanel.dispose();
            }
        });
        container.add(closeCancelButton);

//        pack();
        setVisible(true);
    }
}
