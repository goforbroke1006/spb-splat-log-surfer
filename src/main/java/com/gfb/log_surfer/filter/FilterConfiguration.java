package com.gfb.log_surfer.filter;

import java.io.File;

public class FilterConfiguration {
    public static final String DEFAULT_EXTENSION = "log";

    private File targetDirectory = new File(System.getProperty("user.home"));
    private String query;
    private boolean regex = false;
    private String[] extensions = new String[]{DEFAULT_EXTENSION};

    public File getTargetDirectory() {
        return targetDirectory;
    }

    public FilterConfiguration setTargetDirectory(File targetDirectory) {
        this.targetDirectory = targetDirectory;
        return this;
    }

    public String getQuery() {
        return query;
    }

    public FilterConfiguration setQuery(String query) {
        this.query = query;
        return this;
    }

    public boolean isRegex() {
        return regex;
    }

    public FilterConfiguration setRegex(boolean regex) {
        this.regex = regex;
        return this;
    }

    public String[] getExtensions() {
        return extensions;
    }

    public FilterConfiguration setExtensions(String[] extensions) {
        this.extensions = extensions;
        return this;
    }
}
