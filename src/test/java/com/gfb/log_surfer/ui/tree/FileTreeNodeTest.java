package com.gfb.log_surfer.ui.tree;

import org.junit.Test;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

import static org.junit.Assert.fail;

public class FileTreeNodeTest {
    @Test
    public void testCreateTree() throws Exception {
        FileTreeNode expectedRoot = new FileTreeNode(new File("/"));
        FileTreeNode homeRoot = new FileTreeNode(new File("/home"));
        expectedRoot.insert(homeRoot, 0);
        FileTreeNode userHomeTreeNode = new FileTreeNode(new File("/home/user01"));
        homeRoot.insert(userHomeTreeNode, 0);
        FileTreeNode logsTreeNode = new FileTreeNode(new File("/home/user01/logs/"));
        userHomeTreeNode.insert(logsTreeNode, 0);
        logsTreeNode.insert(new FileTreeNode(new File("/home/user01/logs/dev.log")), 0);
        logsTreeNode.insert(new FileTreeNode(new File("/home/user01/logs/short-dev-2017-08-02.log")), 1);
        FileTreeNode crmLogsTreeNode = new FileTreeNode(new File("/home/user01/logs/some-crm"));
        logsTreeNode.insert(crmLogsTreeNode, 2);
        crmLogsTreeNode.insert(new FileTreeNode(new File("/home/user01/logs/some-crm/short-dev-2017-08-02.log")), 0);

        List<File> files = new ArrayList<>();
        files.add(new File("/home/user01/logs/dev.log"));
        files.add(new File("/home/user01/logs/short-dev-2017-08-02.log"));
        files.add(new File("/home/user01/logs/some-crm/short-dev-2017-08-02.log"));
        FileTreeNode actualRoot = FileTreeNode.createTree(files);

        assertFileTreeNodeEquals(expectedRoot, actualRoot);
    }

    public static void assertFileTreeNodeEquals(FileTreeNode e, FileTreeNode a) {
        if (!fileTreeDeepEquals(e, a)) {
           fail("Expected 2 FileTreeNodes is equals, but not");
        }
    }

    private static boolean fileTreeDeepEquals(FileTreeNode e, FileTreeNode a) {
        if (e.getChildCount() != a.getChildCount())
            return false;

        if (!e.equals(a))
            return false;

        for (int i = 0; i < e.getChildCount(); i++) {
            FileTreeNode tn = e.getChildAt(i);
            FileTreeNode an = a.getChildAt(i);
            if (!fileTreeDeepEquals(tn, an))
                return false;
        }

        return true;
    }
}