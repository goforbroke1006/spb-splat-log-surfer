package com.gfb.log_surfer.filter;

import org.apache.commons.io.FileUtils;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.*;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class FilesScanner {
    private FilterConfiguration configuration;

    public FilesScanner(FilterConfiguration configuration) {
        this.configuration = configuration;
    }

    public Map<File, List<MatchPoint>> run() {
        final Map<File, List<MatchPoint>> matchPoints = new HashMap<>();

        List<File> unfiltered = (List<File>) FileUtils
                .listFiles(
                        configuration.getTargetDirectory(),
                        configuration.getExtensions(),
                        true
                );

        for (File file : unfiltered) {
            try {
                Scanner scanner = new Scanner(file);

                int lineNum = 0;
                while (scanner.hasNextLine()) {
                    String line = scanner.nextLine();
                    lineNum++;
                    if (!configuration.isRegex()) {
                        matchPoints.put(file, scanWithText(lineNum, line));
                    } else {
                        matchPoints.put(file, scanWithRegex(lineNum, line));
                    }
                }
            } catch (FileNotFoundException e) {
                //handle this
            }
        }

        return matchPoints;
    }

    public List<MatchPoint> scanWithText(int lineNumber, String haystack) {
        List<MatchPoint> r = new ArrayList<>();
        Matcher m = Pattern.compile("(" + configuration.getQuery() + ")").matcher(haystack);
        find(lineNumber, r, m);
        return r;
    }

    public List<MatchPoint> scanWithRegex(int lineNumber, String haystack) {
        List<MatchPoint> r = new ArrayList<>();
        Matcher m = Pattern.compile(configuration.getQuery()).matcher(haystack);
        find(lineNumber, r, m);
        return r;
    }

    private void find(int lineNumber, List<MatchPoint> r, Matcher m) {
        while (m.find()) {
            final MatchPoint point = new MatchPoint();
            point
                    .setLineNumber(lineNumber)
                    .setIndex(m.start())
                    .setLength(m.end() - m.start());
            r.add(point);
        }
    }
}
