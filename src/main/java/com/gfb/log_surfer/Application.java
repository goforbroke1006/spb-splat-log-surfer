package com.gfb.log_surfer;

import com.gfb.log_surfer.filter.FilesScanner;
import com.gfb.log_surfer.filter.FilterConfiguration;
import com.gfb.log_surfer.filter.MatchPoint;
import com.gfb.log_surfer.ui.tree.FileTreeNode;
import com.gfb.log_surfer.view.FilterSettingsPanel;

import javax.swing.*;
import javax.swing.tree.DefaultTreeModel;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.io.File;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

public class Application extends JFrame {

    private FilterConfiguration configuration = new FilterConfiguration();

    private JTree filesTree;

    public static void main(String[] args) {
        new Application();
    }

    public Application() {
        createGUI();
    }

    private void createGUI() {
        Application mainPanel = this;

        setTitle("Log Surfer \\=");
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);

        final Dimension standardSize = new Dimension(960, 600);
        setSize(standardSize);
        setMinimumSize(standardSize);
        setResizable(false);

        Container container = getContentPane();
        container.setLayout(null);

        createMainMenu(mainPanel);

        filesTree = new JTree();
        filesTree.setLocation(10, 10);
        filesTree.setSize(280, 520);
        container.add(filesTree);

        JTextArea fileContentTextArea = new JTextArea();
        final JScrollPane jScrollPane = new JScrollPane(fileContentTextArea);
        jScrollPane.setLocation(300, 10);
        jScrollPane.setSize(650, 520);
        container.add(jScrollPane);

//        updateFilesTree();

        pack();
        setVisible(true);
    }

    private void createMainMenu(final Application mainPanel) {
        JMenuBar mainMenu = new JMenuBar();

        JMenuItem findItem = new JMenuItem("Find");
        findItem.addActionListener(new AbstractAction() {
            @Override
            public void actionPerformed(ActionEvent actionEvent) {
                new FilterSettingsPanel(mainPanel, getConfiguration());
            }
        });
        mainMenu.add(findItem);

        JMenuItem exitItem = new JMenuItem("Exit");
        exitItem.addActionListener(new AbstractAction() {
            @Override
            public void actionPerformed(ActionEvent actionEvent) {
                System.exit(0);
            }
        });
        mainMenu.add(exitItem);

        setJMenuBar(mainMenu);
    }

    public FilterConfiguration getConfiguration() {
        return configuration;
    }

    public void setConfiguration(FilterConfiguration configuration) {
        this.configuration = configuration;
    }

    public void updateFilesTree() {
        new Thread(new Runnable() {
            @Override
            public void run() {
                FilesScanner scanner = new FilesScanner(configuration);
                Map<File, List<MatchPoint>> map = scanner.run();

                ArrayList<File> files = new ArrayList<>();
                for (File file : map.keySet()) {
                    if (map.get(file).size() > 0) files.add(file);
                }
                filesTree.setModel(new DefaultTreeModel(FileTreeNode.createTree(files)));
            }
        }).start();
    }
}
