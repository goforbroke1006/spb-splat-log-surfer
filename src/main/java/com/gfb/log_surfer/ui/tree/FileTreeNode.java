package com.gfb.log_surfer.ui.tree;

import com.sun.istack.internal.NotNull;

import javax.swing.tree.MutableTreeNode;
import javax.swing.tree.TreeNode;
import java.io.File;
import java.util.*;

public class FileTreeNode implements MutableTreeNode {
    private File file;

    private FileTreeNode parent;
    private List<FileTreeNode> children = new ArrayList<>();

    public FileTreeNode(@NotNull File file) {
        this.file = file;
        if (null != file.getParentFile())
            this.parent = new FileTreeNode(file.getParentFile());
    }

    @Override
    public void insert(MutableTreeNode mutableTreeNode, int i) {
        FileTreeNode node = (FileTreeNode) mutableTreeNode;
        node.setParent(this);
        children.add(i, node);
    }

    @Override
    public void remove(int i) {
        children.remove(i);
    }

    @Override
    public void remove(MutableTreeNode mutableTreeNode) {
        FileTreeNode node = (FileTreeNode) mutableTreeNode;
        children.remove(node);
    }

    @Override
    public void setUserObject(Object o) {

    }

    @Override
    public void removeFromParent() {
        parent.remove(this);
    }

    @Override
    public void setParent(MutableTreeNode mutableTreeNode) {
        parent = (FileTreeNode) mutableTreeNode;
    }

    @Override
    public FileTreeNode getChildAt(int i) {
        return children.get(i);
    }

    @Override
    public int getChildCount() {
        return children.size();
    }

    @Override
    public TreeNode getParent() {
        return parent;
    }

    @Override
    public int getIndex(TreeNode treeNode) {
        FileTreeNode node = (FileTreeNode) treeNode;
        return children.indexOf(node);
    }

    @Override
    public boolean getAllowsChildren() {
        return true;
    }

    @Override
    public boolean isLeaf() {
        return children.size() == 0;
    }

    @Override
    public Enumeration children() {
        return (Enumeration) children;
    }

    @Override
    public String toString() {
        final String[] arr = file.getAbsolutePath().split(File.separator);
        return arr.length > 0 ? arr[arr.length - 1] : "/";
    }

    @Override
    public boolean equals(Object o) {
        return o instanceof FileTreeNode
                && file.getAbsolutePath().equals(((FileTreeNode) o).file.getAbsolutePath());
    }

    public static FileTreeNode createTree(List<File> files) {
        FileTreeNode root = null;

        for (File t : files) {
            List<File> chain = new ArrayList<>();
            chain.add(t);
            File lp = t;
            while (lp.getParent() != null) {
                lp = lp.getParentFile();
                chain.add(lp);
            }
            Collections.reverse(chain);

            if (null == root)
                root = new FileTreeNode(chain.get(0));

            chain.remove(0);

            FileTreeNode localNode = root;
            for (File el : chain) {
                FileTreeNode next = new FileTreeNode(el);
                int pos = localNode.getIndex(next);
                if (pos < 0) {
                    pos = localNode.getChildCount();
                    localNode.insert(next, pos);
                }
                localNode = localNode.getChildAt(pos);
            }
        }

        return root;
    }
}
