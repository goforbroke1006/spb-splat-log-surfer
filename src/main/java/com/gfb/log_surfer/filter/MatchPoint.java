package com.gfb.log_surfer.filter;

public class MatchPoint {
    private int lineNumber;
    private int index;
    private int length;

    public MatchPoint() {
    }

    public MatchPoint(int lineNumber, int index, int length) {
        this.lineNumber = lineNumber;
        this.index = index;
        this.length = length;
    }

    public int getLineNumber() {
        return lineNumber;
    }

    public MatchPoint setLineNumber(int lineNumber) {
        this.lineNumber = lineNumber;
        return this;
    }

    public int getIndex() {
        return index;
    }

    public MatchPoint setIndex(int index) {
        this.index = index;
        return this;
    }

    public int getLength() {
        return length;
    }

    public MatchPoint setLength(int length) {
        this.length = length;
        return this;
    }

    @Override
    public boolean equals(Object o) {
        return o instanceof MatchPoint
                && lineNumber == ((MatchPoint) o).getLineNumber()
                && index == ((MatchPoint) o).getIndex()
                && length == ((MatchPoint) o).getLength();

    }
}
